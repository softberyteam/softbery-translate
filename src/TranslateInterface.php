<?php

/**
 * Softbery®Group (http://www.softbery.org/)
 *
 * @link      https://bitbucket.org/softberygroup/softbery-translate for repository
 * @copyright Copyright (c) 2010-2018 Softbery®Group (http://www.softbery.ddns.net)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Softbery\Translate;

interface TranslateInterface
{

}