<?php

/**
 * Softbery®Group (http://www.softbery.org/)
 *
 * @link      https://bitbucket.org/softberygroup/softbery-translate for repository
 * @copyright Copyright (c) 2010-2018 Softbery®Group (http://www.softbery.ddns.net)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Softbery\Translate;

/**
* Translate
*/
class Translate implements TranslateInterface
{
	/**
     * Match parameters.
     *
     * @var array
     */
	protected $params = [];

	/**
     * Dictionary file path.
     *
     * @var string
     */
	protected $languageDictionaryPath;

	/**
     * Language dictionary.
     *
     * @var array
     */
	protected $dictionary = [];

	/**
     * Default language.
     *
     * @var string
     */
	protected $default;

	public function __construct(array $params, $default=null)
	{
		$this->params = $params;
		$this->default = $default;
	}

	public function getTranslate($phrase, $language=null)
	{
		if (($language!=null) && (is_string($language))) {
			$default = $language;
		}
	}

	public function setLanguageDictionaryPath($path, $languageSymbol=null)
	{
		if ($languageSymbol!=null) {
			
		}
		else{
			$this->languageDictionaryPath = $path;
		}
	}

	public function getLanguageDictionaryPath()
	{
		return $this->languageDictionaryPath;
	}
}